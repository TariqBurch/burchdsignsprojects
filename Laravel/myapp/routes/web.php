<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'PageController@indexView'); // Calls the page controller & the @ specifies the exact function to be called
Route::get('/about', 'PageController@aboutView');
Route::get('/services', 'PageController@servicesView');

Route::resource('posts', 'PostsController'); //automatically routes the model and the controller




/* Route::get('/about', function () {
    return view('pages.about'); // You either use dot or slash to specific folder locations
}); */




Auth::routes();
Route::get('/dashboard', 'DashboardController@index');

