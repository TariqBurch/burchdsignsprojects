<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller
{

    // authenicates that user be logged in to view post
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index','show']]); // creates exception to the authentication
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$posts = Post::all(); //Call all the post data and store it
        //$posts = Post::orderby('title', 'desc')->get(); //calls and order data
        $posts = Post::orderby('created_at', 'desc')->paginate(10); // When twe have more than 10 post it will be placed on a next page
        return view('posts.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [ //Validation of input data is done
            'title' => 'required',
            'title' => 'required',
            'cover_image' => 'image|nullable|max:1999'

        ]);

        //handle file upload
        if( $request->hasFile('cover_image')){


            //get file name
            $filenameWithExt = $request->file('cover_image') -> getClientOriginalName();

            //get just file name
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);


            //get file ext
            $extension = $request->file('cover_image') -> getClientOriginalExtension();

            //file name to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension; //makes file unique

            //handle upload

            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);

        } else {

            $fileNameToStore = 'no_image.jpg';
        }

        //Post created using tinker into the database
        $post = new Post;
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->user_id = auth()->user()->id; //Gets the users id and stores it with the post
        $post->cover_image = $fileNameToStore;
        $post->save();

        return redirect('/posts')->with('success','Post Created'); // Redirects back to the blog page

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id); //finds a post by id in the database and gets all the data
        return view('posts.show')->with('post', $post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        //check if the user id matches the post user id
        if(auth()->user()->id !== $post->user_id){
            return redirect('/posts')->with('error', 'Unauthorized Page');
        }

        return view('posts.edit')->with('post', $post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [ //Validation of input data is done
            'title' => 'required',
            'title' => 'required',
            'cover_image' => 'image|nullable|max:1999'

        ]);

        //handle file upload
        if( $request->hasFile('cover_image')){


            //get file name
            $filenameWithExt = $request->file('cover_image') -> getClientOriginalName();

            //get just file name
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);


            //get file ext
            $extension = $request->file('cover_image') -> getClientOriginalExtension();

            //file name to store
            $fileNameToStore = $filename.'_'.time().'.'.$extension; //makes file unique

            //handle upload

            $path = $request->file('cover_image')->storeAs('public/cover_images', $fileNameToStore);

        }


        //Post created using tinker into the database
        $post = Post::find($id); //finds post by id so it can be edited
        $post->title = $request->input('title');
        $post->body = $request->input('body');

        //checks if the cover image was updated
        if($request->hasFile('cover_image')){
            $post->cover_image = $fileNameToStore;
        }
        $post->save();

        return redirect('/posts')->with('success','Post Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $post = Post::find($id);//finds post by id so it can be edited

         //check if the user id matches the post user id
         if(auth()->user()->id !== $post->user_id){
            return redirect('/posts')->with('error', 'Unauthorized Page');
        }

        if($post->cover_image != 'no_image.jpg'){

            //delte image
            Storage::delete('public/cover_images/'.$post->cover_image); //removes image
        }
        $post->delete(); // deletes the post based on id

        return redirect('/posts')->with('success','Post Removed');


    }
}
