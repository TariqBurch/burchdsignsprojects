<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function indexView(){
        $title = 'Welcome to My Application'; /* creating a variable called title */
        return view('pages.index') -> with('title', $title); /* passing the variable title down to the view  */
    }
    public function aboutView(){
        $title = 'About';
        return view('pages.about')-> with('title', $title);
    }
    public function servicesView(){
        $data = array( /* creating an array of data */

            'title' => 'Services',

            'services' => ['Web Design', 'Programming', 'Graphics Design']
        );

        return view('pages.services') -> with($data); /* passing an array down*/
    }
}
