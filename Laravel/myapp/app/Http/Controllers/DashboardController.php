<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_id = auth()->user()->id; //creates variable called user id that to store the cvurrent user id
        $user = User::find($user_id); // searches the database and returns all the post with the current user id
        return view('dashboard')->with('posts', $user->posts); //pushes the all the current user post to the view
    }
}
