@extends('layouts.app')

@section('content')
    <a href="/posts" class="btn btn-light">Go Back</a>
    <h1>{{$post->title}}</h1>
    <img style="100%" src="/storage/cover_images/{{$post->cover_image}}"><br><br>
    <div>
        {{$post->body}}
    </div>
    <hr>
    <small>Written on {{$post->created_at}} by {{$post->user->name}}</small>
    <hr>

    {{-- checks whether the user is a guest and limits control --}}
    @if (!Auth::guest())

        @if (Auth::user()->id == $post->user_id) {{-- checks if the user matches the post user id so they cam edit or delete --}}
            <a href="/posts/{{$post->id}}/edit" class="btn btn-light">Edit</a>

            {{-- this creates a hidden delete from to remove the from selected. We do this because laravel only allow PUT & GET --}}
            {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'float-right']) !!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class' => 'btn btn-danger' ])}}
            {!!Form::close() !!}
        @endif

    @endif

@endsection
