@extends('layouts.app')

@section('content')
    <h1>Posts</h1>
        @if (count($posts) > 0)
        @foreach ($posts as $post)
            <div class="card card-body" >
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <img style="100%" src="/storage/cover_images/{{$post->cover_image}}">
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <h5><a href="/posts/{{$post->id}}">{{ $post->title }}</a></h5> {{-- Calls the show function in the controller --}}
                        <small>Written on {{ $post->created_at }} by {{$post->user->name}}</small><br> {{-- calls the user name from user class and display it  --}}
                        <p>{{ $post->body }}</p>
                    </div>
                </div>

            </div>
        @endforeach
        {{$posts->links()}}
    @else
        <p>No post found</p>
    @endif
@endsection

