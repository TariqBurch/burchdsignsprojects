@extends('layouts.app')

@section('content')
    <h1>Edit Post</h1>
    {!! Form::open(['action' => ['PostsController@update', $post->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!} {{-- creates a form  and pushes it to the PostsController update function--}}
        <div class="form-group">
            {{Form::label('title', 'Title')}}
            {{Form::text('title', $post->title, ['class' => 'form-control', 'placeholder' => 'Title'])}} {{-- loads the title from post passed in --}}

        </div>
        <div class="form-group">
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body', $post->body, ['id' => 'article-ckeditor','class' => 'form-control', 'placeholder' => 'Body Text'])}}

        </div>

        <div class="form-group">
            {{Form::file('cover_image')}} {{-- display the file button --}}
        </div>


        {{Form::hidden('_method', 'PUT')}} {{-- You can't do a PUT request or you have to make it hidden --}}
        {{Form::submit('Submit', ['class' => 'btn btn-primary']) }}
    {!! Form::close() !!}
@endsection
