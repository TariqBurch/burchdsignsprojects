@extends('layouts.app')  {{-- calls the layout --}}


@section('content') {{-- places out coode within the layout through an instance --}}
    <div class="jumbotron text-center">
        <h1>{{ $title }}</h1> {{-- calling the title variable that was passed down --}}
        <p>This is my first app created using Laravel Framework. This is a simple blog website.</p>
        <p><a class="btn btn-primary bth-lg" href="/login" role="button">Login</a> <a class="btn btn-success bth-lg" href="/register" role="button">Register</a></p>

    </div>


@endsection

