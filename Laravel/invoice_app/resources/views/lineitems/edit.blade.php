@extends('layouts.app')

@section('content')
    <div class="container">


        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <a href="/lineitems" class="btn btn-outline-secondary">Go Back</a>
        <br><br>


        <div class="d-flex justify-content-center">
            <div class="card " style="width:30rem;">
                <div class="card-header">
                    <h1>Edit Item# {{ $lineitem->item_no }}</h1>
                </div>
                <div class="card-body">

                    {{ Form::model($lineitem, array('route' => array('lineitems.update', $lineitem->item_no), 'method' => 'PUT','enctype' => 'multipart/form-data')) }}

                        <div class="form-group">
                            {{Form::label('invoice_no', 'Invoice Number')}}
                            {{ Form::text('invoice_no', null, array('class' => 'form-control')) }}
                        </div>
                        <div class="form-group">
                            {{Form::label('scope', 'Scope')}}
                            {{ Form::text('scope', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{Form::label('fee', 'Fee')}}
                            {{ Form::text('fee', null, array('class' => 'form-control')) }}
                        </div>
                        <div class="form-group">

                            {{Form::file('documentURL')}}

                        </div>


                        {{ Form::submit('Edit', ['class' => 'btn btn-primary']) }}

                    {{ Form::close() }}
                </div>
            </div>
        </div>



    </div>

@endsection
