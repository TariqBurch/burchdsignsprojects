
@extends('layouts.app')

@section('content')
    <div class="container">

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <a href="/lineitems" class="btn btn-outline-secondary">Go Back</a>
        <br><br>

        <div class="d-flex justify-content-center">
            <div class="card " style="width:30rem;">
                <div class="card-header">
                    <h1>Create a Line Item</h1>
                </div>
                <div class="card-body">
                    {!! Form::open(['action' => 'LineItemController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}


                        <div class="form-group">
                            {{Form::label('invoice_no', 'Invoice Number')}}
                            {{Form::text('invoice_no', '', ['class' => 'form-control', 'placeholder' => 'Invoice Number'])}}
                        </div>


                        <div class="form-group">
                            {{Form::label('scope', 'Scope')}}
                            {{Form::text('scope', '', ['class' => 'form-control', 'placeholder' => 'Scope'])}}
                        </div>

                        <div class="form-group">
                            {{Form::label('fee', 'Fee')}}
                            {{Form::text('fee', '', ['class' => 'form-control', 'placeholder' => 'Fee'])}}
                        </div>
                        <div class="form-group">
                            {{Form::file('documentURL')}}
                        </div>


                        {{Form::submit('Submit', ['class' => 'btn btn-primary']) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
    <br><br>
@endsection

