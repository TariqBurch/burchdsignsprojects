
@extends('layouts.app')

@section('content')


    <div class="container">

        <div class="d-flex justify-content-start">
            <div class="pr-3">
                <h3>All Line Items</h3>
            </div>

            @can('Create Invoice') {{-- Checks if the user can create --}}
                <div>
                    <a href="/lineitems/create" class="btn btn-small btn-outline-secondary ">ADD NEW</a>
                </div>
            @endcan

        </div>
        <br>

        @if (count($lineitems) > 0)
            <table class="table table-hover table-sm">
            <thead>
                <tr class="font-weight-bold">
                    <td>Item #</td>
                    <td>Scope</td>
                    <td>Attachment</td>
                    <td>Fee</td>
                    <td class="text-center">Actions</td>
                </tr>
            </thead>
            <tbody>
                @foreach($lineitems as $key => $value)

                @php

                    $link = Storage::url($value->documentURL);

                @endphp

                    <tr>
                        <td>{{ $value->item_no }}</td>
                        <td>{{ $value->scope }}</td>
                        <td><a href="{{ $link }}"><img src="https://icons.iconarchive.com/icons/custom-icon-design/mono-general-2/512/document-icon.png" alt="document" width="25" height="25"></a></td>
                        <td>{{ $value->fee }}</td>
                        <td>

                            <div class="d-flex justify-content-center ">

                                @can('Edit Invoice') {{-- Checks if the user can edit --}}

                                <!-- edit this invoice (uses the edit method found at GET /invoice/{id}/edit -->
                                <a class="btn btn-small " href="{{ URL::to('lineitems/' . $value->item_no . '/edit') }}">Edit</a>

                                @endcan

                                <div class="pl-3 ">
                                    @can('Delete Invoice') {{-- Checks if the user can delete --}}

                                        {{ Form::open(array('url' => 'lineitems/' . $value->item_no, 'class' => 'font-weight-light')) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        {{ Form::submit('Delete', array('class' => 'btn btn-small ')) }}
                                        {{ Form::close() }}

                                    @endcan
                                </div>

                        </td>
                    </tr>
                @endforeach
            </tbody>
            </table>
        @else
            <p>You have line items</p>
        @endif


    </div>

@endsection
