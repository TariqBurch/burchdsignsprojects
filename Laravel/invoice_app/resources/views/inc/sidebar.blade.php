<style>
    .sidebar {
        margin: 0;
        padding: 0;
        width: 200px;
        background-color: #f1f1f1;
        position: fixed;
        height: 100%;
        overflow: auto;
    }

    .sidebar a {
        display: block;
        color: black;
        padding: 16px;
        text-decoration: none;
    }

    .sidebar a.active {
        background-color: rgb(6, 99, 187);
        color: white;
    }

    .sidebar a:hover:not(.active) {
        background-color:rgb(6, 99, 187);
        color: white;
    }

</style>

<div class="sidebar">
    <br><br>
     <!-- Authentication Links -->
     @guest

        <a class="active" href="{{ route('login') }}">{{ __('Login') }}</a>

        @if (Route::has('register'))

            <a href="{{ route('register') }}">{{ __('Register') }}</a>
        @endif
    @else

            <div class="dropdown show">
                <a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                    {{ Auth::user()->name }}

                </a>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                    @can('Admin') {{-- Ensures only admins can see role & permissions --}}
                        <a class="dropdown-item" href="{{ route('roles.index') }}" >
                            {{ __('Role') }}
                        </a>

                        <a class="dropdown-item" href="{{ route('permissions.index') }}" >
                            {{ __('Permision') }}
                        </a>
                    @endcan

                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
            <br><br><br>
            <div>
                @can('Admin') {{-- Ensures only admins can see all invoices & lineitems --}}
                    <a href="/invoices">Invoices</a>
                    <a href="/lineitems">Line Items</a>
                @endcan
            </div>

    @endguest
</div>
