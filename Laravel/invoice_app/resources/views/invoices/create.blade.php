
@extends('layouts.app')

@section('content')
    <div class="container ">


        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <a href="/invoices" class="btn btn-outline-dark">Go Back</a>
        <br><br>

        <div class="d-flex justify-content-center">
            <div class="card " style="width:30rem;">
                <div class="card-header">
                    <h1>Create a Invoice</h1>
                </div>

                <div class="card-body">
                    {!! Form::open(['action' => 'InvoiceController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                        <div class="form-group">
                            {{Form::label('client_name', 'Client Name')}}
                            {{Form::text('client_name', '', ['class' => 'form-control', 'placeholder' => 'Client Name'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('client_address', 'Client Address')}}
                            {{Form::text('client_address', '', ['class' => 'form-control', 'placeholder' => 'Client Address'])}}
                        </div>
                        <div class="form-group">
                            {{Form::label('notes', 'Notes')}}
                            {{Form::textarea('notes', '', ['id' => 'article-ckeditor','class' => 'form-control', 'placeholder' => 'Notes'])}}
                        </div>
                        <div class="form-group"> {{-- using jquery date picker to get date --}}
                            {{Form::label('date', 'Date')}}
                            {{ Form::text('date', '', ['id' => 'datepicker','class' => 'form-control'])}}

                        </div>
                        <div class="form-group">
                            {{Form::label('access_code', 'Access Code')}}
                            {{Form::text('access_code', '', ['class' => 'form-control', 'placeholder' => 'Access Code'])}}
                        </div>

                        {{Form::submit('Submit', ['class' => 'btn btn-primary']) }}
                    {!! Form::close() !!}
                </div>

            </div>

        </div>

    </div>
    <br><br>



@endsection

