@extends('layouts.app')

@section('content')

    <div class="container">
        @php
            $total=0;

        @endphp

        <a href="/invoices" class="btn btn-outline-dark">Go Back</a>
        <br><br>
        <div class="container">
            {{-- Invoice Header --}}
            <div>
                <h6><strong>Invoice#</strong> {{ $invoice->first()->invoice_no }}</h6>
                <h6 class="float-right"><strong>Invoice Date:</strong> {{ $invoice->first()->date }}</h6>
            </div>
            <br><br>
            <div>
                <h6><Strong>{{ $invoice->first()->client_name }}</Strong></h6>
                <p>{{ $invoice->first()->client_address }}</p>
            </div>

            {{-- Invoice table --}}
            @if (count($invoice) > 0) {{-- checks if the invoice table is empty --}}
                <table class="table table-hover">
                    <thead class="font-weight-bold">
                        <tr>
                            <td>Item #</td>
                            <td>Scope</td>
                            <td>Attachment</td>
                            <td>Fee</td>

                        </tr>
                    </thead>
                    <tbody>

                        @foreach($invoice as $key => $value)

                        @php
                            /* Calculate the total for the invoice */
                            $total = $total + $value->fee;

                            $link = Storage::url($value->documentURL);

                        @endphp



                            <tr>
                                <td>{{ $value->item_no }}</td>
                                <td>{{ $value->scope }}</td>
                                <td><a href="{{ $link }}"><img src="https://icons.iconarchive.com/icons/custom-icon-design/mono-general-2/512/document-icon.png" alt="document" width="25" height="25"></a></td>
                                <td>{{ $value->fee}}</td>

                            </tr>


                        @endforeach
                    </tbody>
                </table>
        @else
            <p>You have line items</p>
        @endif

            <div>

                <h5 class="float-right">Invoice Total <strong>${{ $total }}</strong></h5>
            </div>

        </div>

    </div>

@endsection
