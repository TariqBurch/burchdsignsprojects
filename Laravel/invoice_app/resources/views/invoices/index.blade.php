
@extends('layouts.app')

@section('content')
    <div class="container">

        @php

            $total=0;
            $totalArr = collect([]);
            $amount = 0;
            $indx = 0;

            $temp = $invoices->unique('invoice_no'); /* Removes the reducdancies in the invoice table  */

            $temp2 = $invoices->groupBy('invoice_no'); /* Groups the invoices based on invoice no */

            /* Loop through to create a temp array to the total amount for each group */
            foreach ($temp2 as $key => $value) {
                $tempTotal = 0;



                foreach ($value as $key2 => $innervalue) {
                    $tempTotal = $tempTotal + $innervalue->fee;

                }
                $totalArr->add($tempTotal);


            }


        @endphp

        <div class="d-flex justify-content-start">
            <div class="pr-3">
                <h3>All Invoices</h3>
            </div>

            @can('Create Invoice') {{-- Checks if the user can create --}}
                <div>
                    <a href="/invoices/create" class="btn btn-small btn-outline-secondary ">ADD NEW</a>
                </div>
            @endcan

        </div>

        <br><br>


        @if (count($invoices) > 0) {{-- Checks if invoices is empty --}}
            <table class="table table-hover table-sm width:25%">
            <thead class="font-weight-bold">
                <tr>
                    <td>Invoice #</td>
                    <td>Client Name</td>
                    <td>Amount</td>
                    <td class="text-center">Actions</td>
                </tr>
            </thead>
            <tbody>
            @foreach($temp as $key => $value)

                <tr>
                    {{-- creates a link to the invoice line items --}}
                    <td><a href="{{ URL::to('invoices/' . $value->invoice_no) }}">{{ $value->invoice_no }}</a></td>
                    <td>{{ $value->client_name }}</td>
                    <td>{{ $totalArr[$indx] }}</td>
                    <td>

                        <div class="d-flex justify-content-center ">

                            @can('Edit Invoice') {{-- Checks if the user can edit --}}

                            <!-- edit this invoice (uses the edit method found at GET /invoice/{id}/edit -->
                            <a class="btn btn-small " href="{{ URL::to('invoices/' . $value->invoice_no . '/edit') }}">Edit</a>

                            @endcan

                            <div class="pl-3 ">
                                @can('Delete Invoice') {{-- Checks if the user can delete --}}

                                <!-- delete the invoice (uses the destroy method DESTROY /invoice/{id} -->
                                {{ Form::open(array('url' => 'invoices/' . $value->invoice_no, 'class' => 'font-weight-light')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::submit('Delete', array('class' => 'btn btn-small ')) }}
                                {{ Form::close() }}

                                @endcan
                            </div>
                        </div>

                    </td>
                </tr>

                @php
                 $indx = $indx + 1;

                @endphp

            @endforeach
            </tbody>
            </table>
        @else
            <p>You have invoices</p>
        @endif


    </div>

@endsection
