@extends('layouts.app')

@section('content')
    <div class="container ">


        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <a href="/invoices" class="btn btn-outline-dark">Go Back</a>
        <br><br>

        <div class="d-flex justify-content-center">

            <div class="card " style="width:30rem;">

                <div class="card-header">
                    <h1>Edit {{ $invoice->client_name }} Invoice</h1>
                </div>
                <div class="card-body">
                    {{ Form::model($invoice, array('route' => array('invoices.update', $invoice->invoice_no), 'method' => 'PUT')) }}

                        <div class="form-group">
                            {{ Form::label('client_name', 'Client Name') }}
                            {{ Form::text('client_name', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('client_address', 'Client Address') }}
                            {{ Form::text('client_address', null, array('class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('notes', 'Notes') }}
                            {{ Form::text('notes', null, array('id' => 'article-ckeditor','class' => 'form-control')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('date', 'Date') }}
                            {{ Form::text('date', null, array('id' => 'datepicker','class' => 'form-control'))}}

                        </div>
                        <div class="form-group">
                            {{ Form::label('access_code', 'Access Code') }}
                            {{ Form::text('access_code', null, array('class' => 'form-control')) }}
                        </div>


                        {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}

                    {{ Form::close() }}
                </div>
            </div>

        </div>

    </div>
    <br><br>

@endsection
