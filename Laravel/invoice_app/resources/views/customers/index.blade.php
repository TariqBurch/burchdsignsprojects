@extends('layouts.app')

@section('content')
<div class="container">



    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-8">

            {{-- Back button to home page --}}
            <a href="/home" class="btn btn-light">Go Back</a>
            <br><br>

            <div class="card">
                {{-- Display user name  --}}
                <div class="card-header text-center"><h3> Search for Invoice</h3></div>

                <div class="card-body">


                    <div>

                        {{-- Customer Form: collect the invoice no and access code to find the customer invoice --}}
                        {!! Form::open(['action' => 'CustomerController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
                            <div class="form-group">
                                {{Form::label('invoice_no', 'Invoice Number')}}
                                {{Form::text('invoice_no', '', ['class' => 'form-control', 'placeholder' => 'Invoice Number'])}}
                            </div>

                            <div class="form-group">
                                {{Form::label('access_code', 'Access Code')}}
                                {{Form::text('access_code', '', ['class' => 'form-control', 'placeholder' => 'Access Code'])}}
                            </div>

                            <br>
                            <div class=" d-flex justify-content-center">
                                {{Form::submit('Submit', ['class' => 'btn btn-primary']) }}
                            </div>

                        {!! Form::close() !!}


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
