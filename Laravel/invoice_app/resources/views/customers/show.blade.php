@extends('layouts.app')

@section('content')

    <div class="container">

        {{-- Intialise total variable to get the invoice total --}}
        @php
            $total=0;

        @endphp

        {{-- Back button to home page --}}
        <a href="/customers" class="btn btn-outline-dark">Go Back</a>
        <br><br>

        <div class="container">
            {{-- Customer Invoice Header --}}
            <div>
                <h6><strong>Invoice#</strong> {{ $invoice->first()->invoice_no }}</h6>
                <h6 class="float-right"><strong>Invoice Date:</strong> {{ $invoice->first()->date }}</h6>
            </div>
            <br><br>
            <div>
                <h6><Strong>{{ $invoice->first()->client_name }}</Strong></h6>
                <p>{{ $invoice->first()->client_address }}</p>
            </div>

            {{-- Customer Invoice Table --}}
            @if (count($invoice) > 0) {{-- Checks if invoice array is empty --}}

                <table class="table table-hover">
                <thead>
                    <tr>
                        <td>Item #</td>
                        <td>Scope</td>
                        <td>Attachment</td>
                        <td>Fee</td>
                    </tr>
                </thead>
                <tbody>

                    {{-- Loops through invoice array for lineitems --}}
                    @foreach($invoice as $key => $value)

                        {{-- Adds the value fee to the total --}}
                        @php

                            $total = $total + $value->fee;
                            $link = Storage::url($value->documentURL);

                        @endphp
                        <tr>
                            <td>{{ $value->item_no }}</td>
                            <td>{{ $value->scope }}</td>
                            <td><a href="{{ $link }}"><img src="https://icons.iconarchive.com/icons/custom-icon-design/mono-general-2/512/document-icon.png" alt="document" width="25" height="25"></a></td>
                            <td>{{ $value->fee}}</td>

                        </tr>

                    @endforeach
                </tbody>
                </table>
            @else
                <p>You have line items</p>
            @endif

            <div>
                {{-- Displays invoice total --}}
                <h5 class="float-right">Invoice Total <strong>${{ $total }}</strong></h5>
            </div>

        </div>

    </div>

@endsection
