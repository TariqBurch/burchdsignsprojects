
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit-no">

        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

        <link rel="icon" href="/docs/4.1/assets/img/favicons/favicon.ico">

        <link rel="canonical" href="https://getbootstrap.com/docs/4.1/examples/cover/">

        <!-- Bootstrap core CSS -->
        <link href="../../dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Grocery Mart') }}</title>

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Tangerine">


        <style>

            .w3-tangerine {
                font-family: "Tangerine", serif;
                color: black;
            }
            .w3-regular{
                color:black;
            }
            .cover-image {
                background-image: url('https://www.thebalancesmb.com/thmb/gDGwxUG-ChTGW59R2QszYZoXuOQ=/2122x1194/smart/filters:no_upscale()/GettyImages-514235870-2-5718469c3df78c3fa2dea60d.jpg');/* Image is from google*/
                background-color: #cccccc;
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
                height: 100vh;
                width: 100vh;
        }
        </style>
    </head>

    <body class="text-center cover-image muted">
        <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">

            {{-- Page Header --}}
            <header class="masthead mb-auto"></header>

            {{-- Page Main Body --}}
            <main class="inner cover" role="main">
                <div class="card border-dark mb-3 text-center" style="width: 40rem;">
                    <div class="card-body">
                        <h1 class="display-1 w3-tangerine">Welcome to Le-Fancy Invoice System</h1>
                        <p class="w3-regular blockquote-footer font-italic ml-5" >all your fancy invoice needs and more...</p>

                    </div>
                    <div class="card-footer">
                        <a href="/home" class="btn btn-lg btn-dark">Enter</a>
                    </div>

                </div>
            </main>

            {{-- Page Footer --}}
            <footer class="mastfoot mt-auto font-small text-black">
                <div class="inner">
                    <div class="footer-copyright text-center py-3">© 2020 Copyright:
                    <a href="#">Burch Dsigns</a>

                </div>
            </footer>
        </div>



    </body>
</html>
