@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> {{ Auth::user()->name }} Dashboard</div>

                <div class="card-body">

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status','Success') }}
                        </div>
                    @endif


                    @can('Admin') {{-- Admin dashboard--}}

                        You are logged in admin!!


                    @else {{-- Customer dashboard --}}

                        You are logged in customer!!
                        <br><br>
                        <a class="btn btn-primary d-flex justify-content-center" href="/customers">My Invoices</a>

                    @endcan

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
