<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Line_Item;
use Illuminate\Http\Request;


class InvoiceController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth'); // creates exception to the authentication
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lineitems= Line_Item::all();
        $invoice= Invoice::all();
        $merged = $invoice->merge($lineitems);



        return view('invoices.index')->with('invoices', $merged);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('invoices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         // validate
         $todayDate = date('m/d/Y');


        $this->validate($request, [ //Validation of input data is done



            'client_name' => 'required',
            'client_address' => 'required',
            'notes' => 'required',
            'date' => 'date_format:m/d/Y|before_or_equal:'.$todayDate,
            'access_code' => 'required'
        ]);


        //Post created using tinker into the database
        $invoice = new Invoice;
        $invoice->client_name = $request->input('client_name');
        $invoice->client_address = $request->input('client_address');
        $invoice->notes = $request->input('notes');
        $invoice->date = $request->input('date');
        $invoice->access_code = $request->input('access_code');
        $invoice->save();

        return redirect('/invoices')->with('success','Invoice # '.$invoice->invoice_no.' has been created. Once line items are added, it will be shown on this view. Please go to line items tab to add.');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $lineitems= Line_Item::all();
        $invoices= Invoice::all();
        $merged = $invoices->merge($lineitems);

        //searches for all the related line items by invoice no
        $temp = $merged->intersect(Line_Item::whereIn('invoice_no',[$id])->get());

        if ($temp->isEmpty()) {

            $invoice=collect([]);
            $invoice->add(Invoice::find($id));

        // show the view and pass the invoice to it
            return view('invoices.show')->with('invoice', $invoice);

        } else {

            // show the view and pass the invoice to it
            return view('invoices.show')->with('invoice',$temp);
        }



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          // get the invoice
        $invoice = Invoice::find($id);

        // show the view and pass the invoice to it
        return view('invoices.edit')->with('invoice', $invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         // validate
         $todayDate = date('m/d/Y');


        $this->validate($request, [ //Validation of input data is done



            'client_name' => 'required',
            'client_address' => 'required',
            'notes' => 'required',
            'date' => 'date_format:m/d/Y|before_or_equal:'.$todayDate,
            'access_code' => 'required'
        ]);

        //Post created using tinker into the database
        $invoice = Invoice::find($id);
        $invoice->client_name = $request->input('client_name');
        $invoice->client_address = $request->input('client_address');
        $invoice->notes = $request->input('notes');
        $invoice->date = $request->input('date');
        $invoice->access_code = $request->input('access_code');
        $invoice->save();

        return redirect('/invoices')->with('success','Invoice updated!');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $invoice = Invoice::find($id);
        $invoice->delete(); // deletes the post based on id

        return redirect('/invoices')->with('success','Invoice Removed');

    }
}
