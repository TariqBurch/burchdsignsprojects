<?php

namespace App\Http\Controllers;

use App\Invoice;
use App\Line_Item;
use Illuminate\Http\Request;

class LineItemController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth'); // creates exception to the authentication
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lineitems= Line_Item::all();
        return view('lineitems.index')->with('lineitems', $lineitems);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $invoices= Invoice::all();
        $invoiceArr = collect([]);

        foreach ($invoices as $key => $value) {

            $invoiceArr->add($value->invoice_no);
        }



        return view('lineitems.create')->with('invoices', $invoiceArr);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          // validate


          $this->validate($request, [ //Validation of input data is done
            'invoice_no' => 'required|exists:invoices,invoice_no',
            'scope' => 'required',
            'fee' => 'required',
            'documentURL' =>  'mimes:pdf|required|max:2048',

        ]);

        $path = $request->file('documentURL')->store('documents'); //finds the path of the file and store it in documents folder

        //Post created using tinker into the database
        $lineitem = new Line_Item();
        $lineitem->invoice_no = $request->input('invoice_no');
        $lineitem->scope = $request->input('scope');
        $lineitem->fee = $request->input('fee');
        $lineitem->documentURL = $path;
        $lineitem->save();

        return redirect('/lineitems')->with('success','Line Item  # '.$lineitem->item_no.' has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
           // get the invoice
           $lineitem = Line_Item::find($id);


           // show the view and pass the invoice to it
           return view('lineitems.edit')->with('lineitem', $lineitem);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate


        $this->validate($request, [ //Validation of input data is done
            'invoice_no' => 'required|exists:invoices,invoice_no',
            'scope' => 'required',
            'fee' => 'required',
            'documentURL' =>  'required|mimes:pdf|max:2048',

        ]);

        $path = $request->file('documentURL')->store('documents'); //finds the path of the file and store it in documents folder

        //Post created using tinker into the database
        $lineitem = Line_Item::find($id);
        $lineitem->invoice_no = $request->input('invoice_no');
        $lineitem->scope = $request->input('scope');
        $lineitem->fee = $request->input('fee');
        $lineitem->documentURL = $path;
        $lineitem->save();

        return redirect('/lineitems')->with('success','Line Item  # '.$lineitem->item_no.' has been updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lineitem = Line_Item::find($id);
        $lineitem->delete(); // deletes the post based on id

        return redirect('/lineitems')->with('success','Line Item Removed');

    }
}
