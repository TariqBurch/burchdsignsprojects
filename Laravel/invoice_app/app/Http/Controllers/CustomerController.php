<?php

namespace App\Http\Controllers;


use App\Line_Item;
use App\Invoice;
use Illuminate\Http\Request;

class CustomerController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $invoices= Invoice::all();
        $invoiceArr = collect([]);

        foreach ($invoices as $key => $value) {

            $invoiceArr->add($value->invoice_no);
        }


        return view('customers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [ //Validation of input data is done

            'invoice_no' => 'required|exists:invoices,invoice_no',
            'access_code' => 'required'
        ]);


        $lineitems= Line_Item::all();
        $invoices= Invoice::all();
        $merged = $invoices->merge($lineitems);

        //searches for all the related line items by invoice no
        $temp = $merged->intersect(Line_Item::whereIn('invoice_no',[$request->input('invoice_no')])->get());

        if ($temp->isEmpty()) {

            $invoice=collect([]);
            $invoice->add(Invoice::find($request->input('invoice_no')));

            foreach ($invoice as $key => $value) {

                if ($value->access_code == $request->input('access_code')) {
                    return view('customers.show')->with('invoice',$invoice);
                }
            }

            return redirect()->back()->with('error','Incorrect Password');



        } else {

            foreach ($temp as $key => $value) {



                if ($value->invoice->access_code == $request->input('access_code')) {
                    return view('customers.show')->with('invoice',$temp);
                }
            }

            return redirect()->back()->with('error','Incorrect Password');
        }

        return redirect()->back()->with('error','Error');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
