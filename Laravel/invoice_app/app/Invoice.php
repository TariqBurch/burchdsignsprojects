<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

    public $primaryKey = 'invoice_no';


    public function lineitems(){
        return $this->hasMany(Line_Item::class);
    }


}
