<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Line_Item extends Model
{
    //Table Name --Very Important
    protected $table = 'line__items';

    public $primaryKey = 'item_no';


    public function invoice()
    {
        return $this->belongsTo(Invoice::class,'invoice_no');
    }
}
