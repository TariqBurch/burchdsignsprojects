<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LineItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('line__items')->insert([

            'invoice_no' => 1,
            'scope' => 'Tester',
            'fee' => 30000.00,
            'documentURL' => 'test.com'

        ]);
    }
}
