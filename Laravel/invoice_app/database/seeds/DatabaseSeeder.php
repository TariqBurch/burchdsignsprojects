<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UserSeeder::class);
        $this->call(InvoicesTableSeeder::class);
        $this->call(LineItemTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(ModelRolesTableSeeder::class);
        $this->call(RolePermissionTableSeeder::class);
    }
}
