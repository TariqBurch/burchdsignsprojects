<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([

            'id' => 1,
            'name' => 'Create Invoice',
            'guard_name' => 'web',


        ]);

        DB::table('permissions')->insert([

            'id' => 2,
            'name' => 'Edit Invoice',
            'guard_name' => 'web',


        ]);
        DB::table('permissions')->insert([

            'id' => 3,
            'name' => 'Delete Invoice',
            'guard_name' => 'web',


        ]);

        DB::table('permissions')->insert([

            'id' => 4,
            'name' => 'Create Line Item',
            'guard_name' => 'web',


        ]);

        DB::table('permissions')->insert([

            'id' => 5,
            'name' => 'Edit Line Item',
            'guard_name' => 'web',


        ]);

        DB::table('permissions')->insert([

            'id' => 6,
            'name' => 'Delete Line Item',
            'guard_name' => 'web',


        ]);

        DB::table('permissions')->insert([

            'id' => 7,
            'name' => 'Admin',
            'guard_name' => 'web',


        ]);


    }
}
