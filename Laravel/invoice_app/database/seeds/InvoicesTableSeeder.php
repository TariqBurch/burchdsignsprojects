<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InvoicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('invoices')->insert([

            'client_name' => 'Samuel James',
            'client_address' => '506- New way Drive, Kingston 17, Jamaica',
            'notes' => 'Tester',
            'date' => '06/06/2019',
            'access_code' => '1234'
        ]);
    }
}
