<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('line__items', function (Blueprint $table) {
            $table->increments('item_no');

            $table->integer('invoice_no')->unsigned();
            $table->foreign('invoice_no')
                ->references('invoice_no')->on('invoices')
                ->onDelete('cascade');

            $table->string('scope');
            $table->float('fee');
            $table->string('documentURL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('line__items');
    }
}
