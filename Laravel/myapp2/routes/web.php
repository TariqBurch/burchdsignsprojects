<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', function () {
    return view('welcome');
});
Route::get('/index', ['uses' => 'PageController@indexView', 'as' => 'page.index']);
Route::get('/services', ['uses' => 'PageController@servicesView', 'as' => 'page.services']);
Route::get('/about', ['uses' => 'PageController@aboutView', 'as' => 'page.about']);
Route::get('/dashboard', ['uses' => 'PageController@dashView', 'as' => 'page.dashboard']);


Route::get('/home', 'HomeController@index')->name('home');
