<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function indexView(){

        return view('pages.index');
    }
    public function servicesView(){

        return view('pages.services');
    }
    public function aboutView(){

        return view('pages.about');
    }
    public function dashView(){

        return view('dashboard');
    }





}
