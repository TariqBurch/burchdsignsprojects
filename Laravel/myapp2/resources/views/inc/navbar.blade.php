<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/">BD Grocery Mart</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="/home">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Services
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Pickup</a>
            <a class="dropdown-item" href="#">Delivery</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="/services">Services</a>
          </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/about">About</a>
          </li>
      </ul>
      <form class="form-inline my-2 my-lg-0">

        <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Login</button>
        <button class="btn btn-outline-dark my-2 my-sm-0 ml-3" type="submit">Register</button>
      </form>
    </div>
</nav>
